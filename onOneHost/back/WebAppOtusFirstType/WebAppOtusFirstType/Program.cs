var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSpaStaticFiles(configuration => configuration.RootPath ="ClientApp");
var app = builder.Build();

app.UseSpaStaticFiles();
// Configure the HTTP request pipeline.
/*
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}*/

app.UseSpa(spa => spa.Options.SourcePath = "ClientApp");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();